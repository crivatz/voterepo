package logger;

import java.util.Arrays;
import java.util.List;

public class Log {

    public static void log(Exception e) {
        writeErrorLine(e.toString());
        String stackTrace = extractStackTrace(e);
        writeErrorLine(stackTrace);
    }

    private static String extractStackTrace(Exception e) {
        String stackTrace = "<--------------------------------------------->";
        List<StackTraceElement> stackTraceElements = Arrays.asList(e.getStackTrace());
        for (StackTraceElement element : stackTraceElements) {
            String elementString = element.toString();
            stackTrace = elementString + "\n" + stackTrace;
        }
        stackTrace = "<--------------------STACK-------------------->\n" + stackTrace;
        return stackTrace;
    }

    public static void writeErrorLine(String s) {
        System.err.println(s != null ? s : "NULL");
    }

    public static void writeMessage(String s) {
        System.out.println(s != null ? s : "NULL");
    }
    
    public static void writeMessage(Object o) {
        System.out.println(o != null ? String.valueOf(o) : "NULL");
    }
}
