package model.entity;

public class Vote extends AbstractEntity {

  private Choice choice;
  private String nickname;

  public Vote() {
  }

  public Vote(Choice choice, String nickname) {
    this.choice = choice;
    this.nickname = nickname;
  }

  public Vote(int id, Choice choice, String nickname) {
    this.id = id;
    this.choice = choice;
    this.nickname = nickname;
  }

  public Choice getChoice() {
    return choice;
  }

  public void setChoice(Choice choice) {
    this.choice = choice;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  @Override
  public String toString() {
    return "Vote{" + "id=" + id + ", choice=" + choice + ", nickname=" + nickname + '}';
  }
}
