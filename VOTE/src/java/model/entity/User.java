package model.entity;

public class User extends AbstractEntity {

  private String password;
  private String email;

  public User() {
  }

  public User(String password, String email) {
    this.password = password;
    this.email = email;
  }

  public User(int id, String password, String email) {
    this.id = id;
    this.password = password;
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "User{" + "id=" + id + ", password=" + password + ", email=" + email + '}';
  }
}
