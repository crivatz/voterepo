package model.entity.dao;

import controller.DatabaseConnectionController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import logger.Log;
import model.entity.AbstractEntity;

public abstract class AbstractDAO<T extends AbstractEntity> {

  private final Connection connection;

  public abstract boolean create(T t) throws SQLException;

  public abstract boolean delete(T t) throws SQLException;

  public abstract List<T> getAll() throws SQLException;

  public Connection getConnection() {
    return connection;
  }

  public PreparedStatement getPreparedStatement(String query) throws SQLException {
    return connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
  }

  public AbstractDAO() {
    this.connection = DatabaseConnectionController.getInstance().getConnection();
  }

  public boolean finishCreation(T entity, ResultSet keys) throws SQLException {
    if (keys.next()) {
      entity.setId(keys.getInt(1));
      Log.writeMessage("ENTITY CREATED: " + String.valueOf(entity));
      return true;
    } else {
      return false;
    }
  }
}
