package model.entity.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import logger.Log;
import model.entity.Choice;
import model.entity.User;
import model.entity.Vote;

public class VoteDAO extends AbstractDAO<Vote> {

    @Override
    public boolean create(Vote vote) {
        try {
            String sql = "insert into vote (id, choice, nickname) "
                    + "values (NULL, ?, ?)";

            PreparedStatement statement = getPreparedStatement(sql);

            statement.setInt(1, vote.getChoice().getId());
            statement.setString(2, vote.getNickname());

            statement.executeUpdate();

            return finishCreation(vote, statement.getGeneratedKeys());
        } catch (SQLException ex) {
            Log.log(ex);
            return false;
        }
    }

    @Override
    public boolean delete(Vote vote) {
        try {
            String sql = "DELETE FROM vote "
                    + "WHERE id = ?";

            PreparedStatement statement = getPreparedStatement(sql);

            statement.setInt(1, vote.getId());

            int rows = statement.executeUpdate();
            return rows > 0;
        } catch (SQLException ex) {
            Log.log(ex);
            return false;
        }
    }

    @Override
    public List<Vote> getAll() {
        try {
            String sql = "select id, choice, nickname "
                    + "from vote";

            PreparedStatement statement = getPreparedStatement(sql);
            ResultSet results = statement.executeQuery();

            List<Vote> votes = new ArrayList<>();
            while (results.next()) {
                int id = results.getInt("id");
                int choiceId = results.getInt("choice");
                Choice choice = new ChoiceDAO().getChoiceById(choiceId);
                String nickname = results.getString("nickname");
                votes.add(new Vote(id, choice, nickname));
            }
            return votes;
        } catch (SQLException ex) {
            Log.log(ex);
            return new ArrayList<>();
        }
    }

    public Vote getVoteById(int id) {
        try {
            String sql = "select choice, nickname "
                    + "from vote "
                    + "where id = ?";

            PreparedStatement statement = getPreparedStatement(sql);

            statement.setInt(1, id);

            ResultSet results = statement.executeQuery();

            Vote vote = null;
            while (results.next()) {
                int choiceId = results.getInt("choice");
                Choice choice = new ChoiceDAO().getChoiceById(choiceId);
                String nickname = results.getString("nickname");
                vote = new Vote(id, choice, nickname);
            }
            return vote;
        } catch (SQLException ex) {
            Log.log(ex);
            return null;
        }
    }

    public List<Vote> getVotesByChoice(Choice choice) {
        try {
            String sql = "select id, nickname "
                    + "from vote "
                    + "where choice = ?";

            PreparedStatement statement = getPreparedStatement(sql);
            statement.setInt(1, choice.getId());
            ResultSet results = statement.executeQuery();

            List<Vote> votes = new ArrayList<>();
            while (results.next()) {
                int id = results.getInt("id");
                String nickname = results.getString("nickname");
                votes.add(new Vote(id, choice, nickname));
            }
            return votes;
        } catch (SQLException ex) {
            Log.log(ex);
            return new ArrayList<>();
        }
    }

    public boolean deleteVoteByChoiceAndNickname(Choice choice, String nickname) {
        try {
            String sql = "delete from vote "
                    + "where choice = ? "
                    + "and nickname = ?";

            PreparedStatement statement = getPreparedStatement(sql);
            statement.setInt(1, choice.getId());
            statement.setString(2, nickname);

            int rows = statement.executeUpdate();
            return rows > 0;
        } catch (SQLException ex) {
            Log.log(ex);
            return false;
        }
    }
}
