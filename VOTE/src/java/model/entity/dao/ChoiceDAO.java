package model.entity.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import logger.Log;
import model.entity.Choice;
import model.entity.Playground;

public class ChoiceDAO extends AbstractDAO<Choice> {

  @Override
  public boolean create(Choice choice) {
    try {
      String sql = "insert into choice (id, playground, value) "
              + "values (NULL, ?, ?)";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setInt(1, choice.getPlayground().getId());
      statement.setString(2, choice.getValue());

      statement.executeUpdate();

      return finishCreation(choice, statement.getGeneratedKeys());
    } catch (SQLException ex) {
      Log.log(ex);
      return false;
    }
  }

  @Override
  public boolean delete(Choice choice) {
    try {
      String sql = "DELETE FROM choice "
              + "WHERE id = ?";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setInt(1, choice.getId());

      int rows = statement.executeUpdate();
      return rows > 0;
    } catch (SQLException ex) {
      Log.log(ex);
      return false;
    }
  }

  public boolean deleteByPlayground(Playground playground) {
    try {
      String sql = "DELETE FROM choice "
              + "WHERE playground = ?";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setInt(1, playground.getId());

      int rows = statement.executeUpdate();
      return rows > 0;
    } catch (SQLException ex) {
      Log.log(ex);
      return false;
    }
  }

  @Override
  public List<Choice> getAll() {
    try {
      String sql = "select id, playground, value "
              + "from choice";

      PreparedStatement statement = getPreparedStatement(sql);
      ResultSet results = statement.executeQuery();

      List<Choice> choices = new ArrayList<>();
      while (results.next()) {
        int id = results.getInt("id");
        int playgroundId = results.getInt("playground");
        Playground playground = new PlaygroundDAO().getPlaygroundById(playgroundId);
        String value = results.getString("value");
        choices.add(new Choice(id, playground, value));
      }
      return choices;
    } catch (SQLException ex) {
      Log.log(ex);
      return new ArrayList<>();
    }
  }

  public List<Choice> getChoicesByPlayground(Playground playground) {
    try {
      String sql = "select id, value "
              + "from choice "
              + "where playground = ?";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setInt(1, playground.getId());

      ResultSet results = statement.executeQuery();

      List<Choice> choices = new ArrayList<>();
      while (results.next()) {
        int id = results.getInt("id");
        String value = results.getString("value");
        choices.add(new Choice(id, playground, value));
      }
      return choices;
    } catch (SQLException ex) {
      Log.log(ex);
      return new ArrayList<>();
    }
  }

  public Choice getChoiceById(int id) {
    try {
      String sql = "select playground, value "
              + "from choice "
              + "where id = ?";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setInt(1, id);

      ResultSet results = statement.executeQuery();

      Choice choice = null;
      while (results.next()) {
        String value = results.getString("value");
        int playgroundId = results.getInt("playground");
        Playground playground = new PlaygroundDAO().getPlaygroundById(playgroundId);
        choice = new Choice(id, playground, value);
      }
      return choice;
    } catch (SQLException ex) {
      Log.log(ex);
      return null;
    }
  }
}
