package model.entity.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import logger.Log;
import model.entity.User;

public class UserDAO extends AbstractDAO<User> {

  @Override
  public boolean create(User user) {
    try {
      String sql = "insert into user (id, password, email) "
              + "values (NULL, ?, ?)";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setString(1, user.getPassword());
      statement.setString(2, user.getEmail());

      statement.executeUpdate();

      return finishCreation(user, statement.getGeneratedKeys());
    } catch (SQLException ex) {
      Log.log(ex);
      return false;
    }
  }

  @Override
  public boolean delete(User user) {
    try {
      String sql = "DELETE FROM user "
              + "WHERE id = ?";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setInt(1, user.getId());

      int rows = statement.executeUpdate();
      return rows > 0;
    } catch (SQLException ex) {
      Log.log(ex);
      return false;
    }
  }

  public boolean deleteByEmail(String email) {
    try {
      String sql = "DELETE FROM user "
              + "WHERE email = ?";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setString(1, email);

      int rows = statement.executeUpdate();
      return rows > 0;
    } catch (SQLException ex) {
      Log.log(ex);
      return false;
    }
  }

  @Override
  public List<User> getAll() {
    try {
      String sql = "select id, password, email, "
              + "from user";

      PreparedStatement statement = getPreparedStatement(sql);
      ResultSet results = statement.executeQuery();

      List<User> users = new ArrayList<>();
      while (results.next()) {
        int id = results.getInt("id");
        String password = results.getString("password");
        String email = results.getString("email");
        users.add(new User(id, password, email));
      }
      return users;
    } catch (SQLException ex) {
      Log.log(ex);
      return new ArrayList<>();
    }
  }

  public User getUserByEmail(String userEmail) {
    try {
      String sql = "select id, password, email "
              + "from user "
              + "where email = ?";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setString(1, userEmail);

      ResultSet results = statement.executeQuery();

      User user = null;
      while (results.next()) {
        int id = results.getInt("id");
        String password = results.getString("password");
        String email = results.getString("email");
        user = new User(id, password, email);
      }
      return user;
    } catch (SQLException ex) {
      Log.log(ex);
      return null;
    }
  }

  public User getUserById(int id) {
    try {
      String sql = "select id, password, email "
              + "from user "
              + "where id = ?";

      PreparedStatement statement = getPreparedStatement(sql);

      statement.setInt(1, id);

      ResultSet results = statement.executeQuery();

      User user = null;
      while (results.next()) {
        int userId = results.getInt("id");
        String password = results.getString("password");
        String email = results.getString("email");
        user = new User(userId, password, email);
      }
      return user;
    } catch (SQLException ex) {
      Log.log(ex);
      return null;
    }
  }
}
