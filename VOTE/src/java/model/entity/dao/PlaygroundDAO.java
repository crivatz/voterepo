package model.entity.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import logger.Log;
import model.entity.Playground;
import model.entity.User;

public class PlaygroundDAO extends AbstractDAO<Playground> {

    @Override
    public boolean create(Playground playground) {
        try {
            String sql = "insert into playground (id, name, creator, active) "
                    + "values (NULL, ?, ?, ?)";

            PreparedStatement statement = getPreparedStatement(sql);

            statement.setString(1, playground.getName());
            statement.setInt(2, playground.getCreator().getId());
            statement.setBoolean(3, playground.isActive());
            statement.executeUpdate();
            boolean creationFinished = finishCreation(playground, statement.getGeneratedKeys());
            return creationFinished;
        } catch (SQLException ex) {
            Log.log(ex);
            return false;
        }
    }

    @Override
    public boolean delete(Playground playground) {
        try {
            String sql = "DELETE FROM playground "
                    + "WHERE id = ?";

            PreparedStatement statement = getPreparedStatement(sql);

            statement.setInt(1, playground.getId());

            int rows = statement.executeUpdate();
            return rows > 0;
        } catch (SQLException ex) {
            Log.log(ex);
            return false;
        }
    }

    @Override
    public List<Playground> getAll() {
        try {
            String sql = "select id, name, creator, active "
                    + "from playground";

            PreparedStatement statement = getPreparedStatement(sql);
            ResultSet results = statement.executeQuery();
            List<Playground> playgrounds = new ArrayList<>();
            while (results.next()) {
                int id = results.getInt("id");
                String name = results.getString("name");
                int creatorId = results.getInt("creator");
                User creator = new UserDAO().getUserById(creatorId);
                boolean active = results.getBoolean("active");
                playgrounds.add(new Playground(id, name, creator, active));
            }
            return playgrounds;
        } catch (SQLException ex) {
            Log.log(ex);
            return new ArrayList<>();
        }
    }

    public Playground getPlaygroundById(int playgroundId) {
        try {
            String sql = "select name, creator, active "
                    + "from playground "
                    + "where id = ?";

            PreparedStatement statement = getPreparedStatement(sql);

            statement.setInt(1, playgroundId);

            ResultSet results = statement.executeQuery();

            Playground playground = null;
            while (results.next()) {
                String name = results.getString("name");
                int creatorId = results.getInt("creator");
                User creator = new UserDAO().getUserById(creatorId);
                boolean active = results.getBoolean("active");
                playground = new Playground(playgroundId, name, creator, active);
            }
            return playground;
        } catch (SQLException ex) {
            Log.log(ex);
            return null;
        }
    }

    public List<Playground> getPlaygroundsForUser(User user) {
        try {
            if (user == null) {
                return new ArrayList<>();
            }
            String sql = "select id, name, creator, active "
                    + "from playground "
                    + "where creator = ?";

            PreparedStatement statement = getPreparedStatement(sql);
            statement.setInt(1, user.getId());
            ResultSet results = statement.executeQuery();
            List<Playground> playgrounds = new ArrayList<>();
            while (results.next()) {
                int id = results.getInt("id");
                String name = results.getString("name");
                int creatorId = results.getInt("creator");
                User creator = new UserDAO().getUserById(creatorId);
                boolean active = results.getBoolean("active");
                playgrounds.add(new Playground(id, name, creator, active));
            }
            return playgrounds;
        } catch (SQLException ex) {
            Log.log(ex);
            return new ArrayList<>();
        }
    }
}
