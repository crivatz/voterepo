package model.entity;

public class EntityUtils {

    public static boolean stringToBoolean(String string) {
        return "Y".equals(string) || "y".equals(string);
    }

    public static String booleanToString(boolean bool) {
        return bool ? "Y" : "N";
    }
}
