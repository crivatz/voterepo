package model.entity;

public class Playground extends AbstractEntity {

    private String name;
    private User creator;
    private boolean active;

    public Playground() {
    }

    public Playground(int id, String name, User creator, boolean active) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.active = active;
    }

    public Playground(String name, User creator, boolean active) {
        this.name = name;
        this.creator = creator;
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Playground{" + "id=" + id + ", name=" + name + ", creator=" + creator + ", active=" + active + '}';
    }

}
