package model.entity;

public class Choice extends AbstractEntity {

    private Playground playground;
    private String value;

    public Choice() {
    }

    public Choice(int id, Playground playground, String value) {
        this.id = id;
        this.playground = playground;
        this.value = value;
    }

    public Choice(Playground playground, String value) {
        this.playground = playground;
        this.value = value;
    }

    public Playground getPlayground() {
        return playground;
    }

    public void setPlayground(Playground playground) {
        this.playground = playground;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Choice{" + "id=" + id + ", playground=" + playground + ", value=" + value + '}';
    }
}
