package controller.businessControllers;

import model.entity.Playground;
import model.entity.dao.PlaygroundDAO;

public class PlaygroundController {

    PlaygroundDAO playgroundDAO;

    public PlaygroundController() {
        playgroundDAO = new PlaygroundDAO();
    }

    public Playground getPlaygroundById(int id) {
        return playgroundDAO.getPlaygroundById(id);
    }

    public void createPlayground(Playground playground) {
        playgroundDAO.create(playground);
    }

    public void deletePlayground(Playground playground) {
        playgroundDAO.delete(playground);
    }

}
