package controller.businessControllers;

import java.util.List;
import model.entity.Playground;
import model.entity.User;
import model.entity.dao.PlaygroundDAO;

public class PlaygroundsController {

  PlaygroundDAO playgroundDAO;

  public PlaygroundsController() {
    playgroundDAO = new PlaygroundDAO();
  }

  public List<Playground> getPlaygroundsForUser(User user) {
    return playgroundDAO.getPlaygroundsForUser(user);
  }

}
