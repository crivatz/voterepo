package controller.businessControllers;

import java.util.List;
import model.entity.Choice;
import model.entity.Playground;
import model.entity.dao.ChoiceDAO;

public class ChoiceController {

    ChoiceDAO choiceDAO;

    public ChoiceController() {
        choiceDAO = new ChoiceDAO();
    }

    public List<Choice> getChoicesForPlayground(Playground playground) {
        return choiceDAO.getChoicesByPlayground(playground);
    }

    public void createChoice(Choice choice) {
        choiceDAO.create(choice);
    }

    public void removeChoice(Choice choice) {
        choiceDAO.delete(choice);
    }
}
