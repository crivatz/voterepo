package controller.businessControllers;

import model.entity.User;
import model.entity.dao.UserDAO;

public class LoginController {

  public User tryLogin(String email, String password) {
    if (email == null) {
      return null;
    }
    User user = new UserDAO().getUserByEmail(email);
    return user != null && password != null && password.equals(user.getPassword()) ? user : null;
  }

  public User createUser(String email, String password) {
    if (email == null || password == null) {
      return null;
    }

    User user = new User(password, email);

    if (emailIsFree(email)) {
      new UserDAO().create(user);
      return user;
    } else {
      return null;
    }
  }

  public boolean emailIsFree(String email) {
    return email != null && new UserDAO().getUserByEmail(email) == null;
  }

}
