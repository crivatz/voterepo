package controller.businessControllers;

import java.util.List;
import model.entity.Choice;
import model.entity.Playground;
import model.entity.Vote;
import model.entity.dao.ChoiceDAO;
import model.entity.dao.VoteDAO;

public class VoteController {

    VoteDAO voteDAO;

    public VoteController() {
        voteDAO = new VoteDAO();
    }

    public List<Vote> getVotesByChoice(Choice choice) {
        return voteDAO.getVotesByChoice(choice);
    }

    public void createVote(Vote vote) {
        voteDAO.create(vote);
    }

    public void deleteVote(Vote vote) {
        voteDAO.delete(vote);
    }

    public void deleteVoteByChoiceAndNickname(Choice choice, String nickname) {
        voteDAO.deleteVoteByChoiceAndNickname(choice, nickname);
    }
}
