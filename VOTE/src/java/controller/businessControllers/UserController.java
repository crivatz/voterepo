package controller.businessControllers;

import model.entity.User;
import model.entity.dao.UserDAO;

public class UserController {

  private UserDAO userDAO;

  public UserController() {
    userDAO = new UserDAO();
  }

  public User tryLogin(String email, String password) {
    if (email == null) {
      return null;
    }
    User user = userDAO.getUserByEmail(email);
    return user != null && password != null && password.equals(user.getPassword()) ? user : null;
  }

  public User createUser(String email, String password) {
    if (email == null || password == null) {
      return null;
    }

    User user = new User(password, email);

    if (emailIsFree(email)) {
      userDAO.create(user);
      return user;
    } else {
      return null;
    }
  }

  public boolean emailIsFree(String email) {
    return email != null && userDAO.getUserByEmail(email) == null;
  }

}
