package controller.businessControllers;

public class MainController {

    private static MainController singleton;
    private UserController loginController;
    private PlaygroundsController playgroundsController;
    private PlaygroundController playgroundController;
    private ChoiceController choiceController;
    private VoteController voteController;

    private MainController() {
        loginController = new UserController();
        playgroundsController = new PlaygroundsController();
        playgroundController = new PlaygroundController();
        choiceController = new ChoiceController();
        voteController = new VoteController();
    }

    public static MainController getInstance() {
        if (singleton == null) {
            singleton = new MainController();
        }
        return singleton;
    }

    public UserController getLoginController() {
        return loginController;
    }

    public PlaygroundsController getPlaygroundsController() {
        return playgroundsController;
    }

    public PlaygroundController getPlaygroundController() {
        return playgroundController;
    }

    public ChoiceController getChoiceController() {
        return choiceController;
    }

    public VoteController getVoteController() {
        return voteController;
    }

}
