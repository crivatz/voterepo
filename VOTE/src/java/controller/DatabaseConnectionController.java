package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import logger.Log;

public class DatabaseConnectionController {

    private static DatabaseConnectionController singleton;
    private Connection connection;

    public static final String DB_URL = "jdbc:mysql://localhost/vote";
    public static final String DB_USER = "root";
    public static final String DB_PASS = "";

    private DatabaseConnectionController() {
        connect();
    }

    private void connect() {
        try {
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        } catch (SQLException ex) {
            Log.log(ex);
        }
    }

    public static DatabaseConnectionController getInstance() {
        if (singleton == null) {
            singleton = new DatabaseConnectionController();
        }
        return singleton;
    }

    public Connection getConnection() {
        return connection;
    }

}
