package controller;

import controller.businessControllers.UserController;
import controller.businessControllers.MainController;
import java.sql.SQLException;
import java.util.List;
import logger.Log;
import model.entity.Choice;
import model.entity.Playground;
import model.entity.User;
import model.entity.Vote;
import model.entity.dao.ChoiceDAO;
import model.entity.dao.PlaygroundDAO;
import model.entity.dao.UserDAO;
import model.entity.dao.VoteDAO;

public class Main {

    static User testUser = new User("testPassword", "testEmail");
    static Playground testPlayground = new Playground("testPlayground", testUser, true);
    static Choice testChoice = new Choice(testPlayground, "testValue");
    static Vote testVote = new Vote(testChoice, "testNickname");

    public static void main(String... args) {
        try {
            test();
        } catch (Exception ex) {
            Log.log(ex);
        }
    }

    public static void test() throws Exception {
        UserController loginController = MainController.getInstance().getLoginController();
        Log.writeMessage("Should be null: " + loginController.tryLogin("testEmail", "testPassword"));
        Log.writeMessage("Should return user: " + loginController.createUser("testEmail", "testPassword"));
        Log.writeMessage("Should return user: " + loginController.tryLogin("testEmail", "testPassword"));
        Log.writeMessage("Should be null: " + loginController.createUser("testEmail", "testPassword"));
    }

    public void oldTest() {
        UserDAO userOperations = new UserDAO();
        PlaygroundDAO playgroundOperations = new PlaygroundDAO();
        ChoiceDAO choiceOperations = new ChoiceDAO();
        VoteDAO voteOperations = new VoteDAO();

        userOperations.create(testUser);
        playgroundOperations.create(testPlayground);
        choiceOperations.create(testChoice);
        voteOperations.create(testVote);
        Log.writeMessage("Both wrong login: " + MainController.getInstance().getLoginController().tryLogin("wrongEmail", "wrongPassword"));
        Log.writeMessage("Pass wrong login: " + MainController.getInstance().getLoginController().tryLogin("testEmail", "wrongPassword"));
        Log.writeMessage("Both right login: " + MainController.getInstance().getLoginController().tryLogin("testEmail", "testPassword"));
    }
}
