package view.beans;

import controller.businessControllers.MainController;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.entity.Choice;
import model.entity.Playground;

@ManagedBean
@ViewScoped
public class NewPlaygroundView {

    private String playgroundName;
    private String choiceName;
    private Set<String> choices;

    @PostConstruct
    public void init() {
        playgroundName = "";
        choices = new LinkedHashSet<>();
    }

    public void addChoice() {
        if (!"".equals(choiceName) && choiceName != null) {
            choices.add(choiceName);
        }
        this.choiceName = "";
    }

    public void removeChoice(String choice) {
        choices.remove(choice);
    }

    public void save() {
        if ("".equals(playgroundName) || playgroundName == null) {
            return;
        }
        Playground playground = new Playground(playgroundName, SessionUtils.getSessionUser(), true);
        MainController.getInstance().getPlaygroundController().createPlayground(playground);
        choices.stream().map(x -> new Choice(playground, x))
                .forEach(x -> MainController.getInstance().getChoiceController().createChoice(x));
        SessionUtils.redirect("playgrounds");
    }

    public void back() {
        SessionUtils.redirect("playgrounds");
    }

    public String getPlaygroundName() {
        return playgroundName;
    }

    public void setPlaygroundName(String playgroundName) {
        this.playgroundName = playgroundName;
    }

    public String getChoiceName() {
        return choiceName;
    }

    public void setChoiceName(String choiceName) {
        this.choiceName = choiceName;
    }

    public Set<String> getChoices() {
        return choices;
    }

    public void setChoices(Set<String> choices) {
        this.choices = choices;
    }

}
