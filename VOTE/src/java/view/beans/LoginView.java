package view.beans;

import controller.businessControllers.MainController;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.entity.Playground;
import model.entity.User;

@ManagedBean
@RequestScoped
public class LoginView {

    private String email;
    private String password;
    private final FacesMessage failedLoginMessage = new FacesMessage("Incorrect username or password");
    private final FacesMessage playgroundNotFoundMessage = new FacesMessage("Poll not found");
    private String uniqueCode;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void validateLogin() {
        User user = MainController.getInstance().getLoginController().tryLogin(email, password);
        if (user != null) {
            HttpSession session = SessionUtils.getSession();
            session.setAttribute("user", user);
            SessionUtils.redirect("playgrounds");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, failedLoginMessage);
        }
    }

    public String createUser() {
        return "newUser" + SessionUtils.REDIRECT_URL_SUFFIX;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public void goToUniqueCode() {
        if (uniqueCode == null || "".equals(uniqueCode)) {
            return;
        }
        int playgroundId = Integer.valueOf(uniqueCode) / PlaygroundView.PRIME_NUMBER;
        Playground playground = MainController.getInstance().getPlaygroundController().getPlaygroundById(playgroundId);
        if (playground != null) {
            SessionUtils.redirectToPlayground(playground);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, playgroundNotFoundMessage);
        }
    }

}
