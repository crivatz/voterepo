package view.beans;

import controller.businessControllers.UserController;
import controller.businessControllers.MainController;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.entity.User;

@ManagedBean
@RequestScoped
public class NewUserView {

    private String email;
    private String password;
    private final FacesMessage failedLoginMessage = new FacesMessage("Email already exists!");

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void validateNewUser() {
        UserController loginController = MainController.getInstance().getLoginController();
        boolean emailIsFree = loginController.emailIsFree(email);
        if (emailIsFree) {
            User user = loginController.createUser(email, password);
            HttpSession session = SessionUtils.getSession();
            session.setAttribute("user", user);
            SessionUtils.redirect("playgrounds");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, failedLoginMessage);
        }
    }

    public void cancel() {
        SessionUtils.redirect("login");
    }
}
