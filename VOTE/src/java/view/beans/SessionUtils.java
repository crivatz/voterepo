package view.beans;

import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import logger.Log;
import model.entity.Playground;
import model.entity.User;

@ManagedBean
@SessionScoped
public class SessionUtils {

    public static final String REDIRECT_URL_SUFFIX = "?faces-redirect=true";

    public static HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
    }

    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest();
    }

    public static User getSessionUser() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
        if (session.getAttribute("user") == null) {
            return null;
        }
        return (User) session.getAttribute("user");
    }

    public static void redirect(String url) {
        try {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            context.redirect(context.getRequestContextPath() + "/faces/" + url + ".xhtml");
        } catch (IOException ex) {
            Log.log(ex);
        }
    }

    public static void redirectToPlayground(Playground playground) {
        try {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            context.redirect(context.getRequestContextPath()
                    + "/faces/"
                    + "playground.xhtml"
                    + "?uniqueLink="
                    + playground.getId() * PlaygroundView.PRIME_NUMBER);
        } catch (IOException ex) {
            Log.log(ex);
        }
    }
}
