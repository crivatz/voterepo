package view.beans;

import controller.businessControllers.ChoiceController;
import controller.businessControllers.MainController;
import controller.businessControllers.PlaygroundController;
import controller.businessControllers.VoteController;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.entity.Choice;
import model.entity.Playground;
import model.entity.User;
import model.entity.Vote;

@ManagedBean
@ViewScoped
public class PlaygroundsView {

    private User user;
    private List<Playground> playgrounds;
    private Playground selectedPlayground;
    private final FacesMessage failedLoginMessage = new FacesMessage("Incorrect username or password");

    public PlaygroundsView() {
        this.user = SessionUtils.getSessionUser();
        playgrounds = MainController.getInstance().getPlaygroundsController().getPlaygroundsForUser(user);
    }

    public void removePlayground(Playground playground) {
        PlaygroundController playgroundController = MainController.getInstance().getPlaygroundController();
        ChoiceController choiceController = MainController.getInstance().getChoiceController();
        VoteController voteController = MainController.getInstance().getVoteController();

        List<Choice> choices = choiceController.getChoicesForPlayground(playground);
        List<Vote> votes = new ArrayList<>();
        choices.stream().forEach(x -> votes.addAll(voteController.getVotesByChoice(x)));

        votes.stream().forEach(x -> voteController.deleteVote(x));
        choices.stream().forEach(x -> choiceController.removeChoice(x));
        playgroundController.deletePlayground(playground);

        playgrounds.remove(playground);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Playground> getPlaygrounds() {
        return playgrounds;
    }

    public void setPlaygrounds(List<Playground> playgrounds) {
        this.playgrounds = playgrounds;
    }

    public Playground getSelectedPlayground() {
        return selectedPlayground;
    }

    public void setSelectedPlayground(Playground selectedPlayground) {
        this.selectedPlayground = selectedPlayground;
    }

    public void navigateToPlayground() {
        SessionUtils.redirectToPlayground(selectedPlayground);
    }

    public void navigateToNewPlayground() {
        SessionUtils.redirect("newPlayground");
    }

    public String getSessionUser() {
        return SessionUtils.getSessionUser().getEmail();
    }
}
