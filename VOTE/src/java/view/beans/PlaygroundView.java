package view.beans;

import controller.businessControllers.MainController;
import controller.businessControllers.VoteController;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import model.entity.Choice;
import model.entity.Playground;
import model.entity.Vote;

@ManagedBean
@ViewScoped
public class PlaygroundView {

    public static final int PRIME_NUMBER = 199;
    private Playground currentPlayground;
    private List<Choice> choices;
    private List<Vote> allVotes;
    private List<String> nicknames;
    private List<PlaygroundTableRow> rows;
    private Choice selectedChoice;

    @PostConstruct
    public void init() {
        int uniqueLink = Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("uniqueLink"));
        final MainController mainController = MainController.getInstance();
        currentPlayground = mainController.getPlaygroundController().getPlaygroundById(uniqueLink / PlaygroundView.PRIME_NUMBER);
        choices = mainController.getChoiceController().getChoicesForPlayground(currentPlayground);
        allVotes = new ArrayList<>();
        choices.stream().forEach(x -> allVotes.addAll(mainController.getVoteController().getVotesByChoice(x)));
        nicknames = allVotes.stream().map(x -> x.getNickname()).distinct().collect(Collectors.toList());
        Collections.sort(nicknames);
        rows = new ArrayList<>();
        nicknames.stream().forEach(x -> rows.add(new PlaygroundTableRow(x, choices, false)));

        for (PlaygroundTableRow row : rows) {
            String nickname = row.getNickname();
            Map<Choice, Boolean> choiceVoteBindings = row.getChoiceVoteBindings();
            List<Vote> votesForNickname = allVotes.stream().filter(x -> x.getNickname().equals(nickname)).collect(Collectors.toList());
            votesForNickname.stream().forEach(x -> choiceVoteBindings.put(x.getChoice(), Boolean.TRUE));
        }
        PlaygroundTableRow newRow = new PlaygroundTableRow("", choices, true);
        rows.add(newRow);

        HttpSession session = SessionUtils.getSession();
        if (SessionUtils.getSessionUser() == null) {
            session.setAttribute("tempUser", "tempUser");
        }
    }

    public Choice getSelectedChoice() {
        return selectedChoice;
    }

    public void setSelectedChoice(Choice selectedChoice) {
        this.selectedChoice = selectedChoice;
    }

    public Playground getCurrentPlayground() {
        return currentPlayground;
    }

    public void setCurrentPlayground(Playground currentPlayground) {
        this.currentPlayground = currentPlayground;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public List<Vote> getAllVotes() {
        return allVotes;
    }

    public void setAllVotes(List<Vote> allVotes) {
        this.allVotes = allVotes;
    }

    public List<String> getNicknames() {
        return nicknames;
    }

    public void setNicknames(List<String> nicknames) {
        this.nicknames = nicknames;
    }

    public List<PlaygroundTableRow> getRows() {
        return rows;
    }

    public void setRows(List<PlaygroundTableRow> rows) {
        this.rows = rows;
    }

    public void updateVote(String nickname, Choice choice, boolean voted) {
        if ("".equals(nickname) || nickname == null) {
            return;
        }
        for (PlaygroundTableRow row : rows) {
            if (row.getNickname().equals(nickname)) {
                row.getChoiceVoteBindings().put(choice, voted);
            }
        }
        VoteController voteController = MainController.getInstance().getVoteController();
        if (voted) {
            Vote vote = new Vote(choice, nickname);
            voteController.createVote(vote);
        } else {
            voteController.deleteVoteByChoiceAndNickname(choice, nickname);
        }
    }

    public String getUniqueLink() {
        return String.valueOf(currentPlayground.getId() * PlaygroundView.PRIME_NUMBER);

    }

    public void reload() {
        SessionUtils.redirectToPlayground(currentPlayground);
    }

    public void back() {
        SessionUtils.redirect("playgrounds");
    }

    public void toLogin() {
        SessionUtils.redirect("login");
    }

    public boolean isButtonBackVisible() {
        return SessionUtils.getSessionUser() != null;
    }
    
    public int getNumberOfVotesForChoice (Choice choice){
        return MainController.getInstance().getVoteController().getVotesByChoice(choice).size();
    }

}
