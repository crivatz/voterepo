/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.entity.Choice;

/**
 *
 * @author Andrei
 */
public class PlaygroundTableRow {

    private String nickname;
    private Map<Choice, Boolean> choiceVoteBindings;
    private boolean editable;

    public PlaygroundTableRow(String nickname, List<Choice> choices, boolean editable) {
        this.nickname = nickname;
        this.editable = editable;
        choiceVoteBindings = new HashMap<>();
        choices.forEach(x -> choiceVoteBindings.put(x, Boolean.FALSE));
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Map<Choice, Boolean> getChoiceVoteBindings() {
        return choiceVoteBindings;
    }

    public void setChoiceVoteBindings(Map<Choice, Boolean> choiceVoteBindings) {
        this.choiceVoteBindings = choiceVoteBindings;
    }

    public void setVote(Choice choice, Boolean b) {
        choiceVoteBindings.put(choice, b);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

}
