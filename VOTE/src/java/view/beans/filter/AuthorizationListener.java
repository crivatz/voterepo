package view.beans.filter;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;
import model.entity.User;
import view.beans.SessionUtils;

public class AuthorizationListener implements PhaseListener {

    @Override
    public void afterPhase(PhaseEvent event) {
        FacesContext facesContext = event.getFacesContext();
        String currentPage = facesContext.getViewRoot().getViewId();
        boolean isTempUser = isTempUser(currentPage);

        boolean authenthicationNotRequired = currentPage.contains("login")
                || currentPage.contains("newUser")
                || isTempUser;

        User sessionUser = getSessionUser(facesContext);
        if (!authenthicationNotRequired && (sessionUser == null || isTempUser)) {
            SessionUtils.redirect("faces/login");
        }
    }

    private boolean isTempUser(String currentPage) {
        HttpSession session = SessionUtils.getSession();
        boolean isTempUser = currentPage.contains("playground.xhtml")
                && (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("uniqueLink") != null
                || (session != null && session.getAttribute("tempUser") != null));
        return isTempUser;
    }

    private User getSessionUser(FacesContext facesContext) {
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        User sessionUser = null;
        if (session != null && session.getAttribute("user") != null) {
            sessionUser = (User) session.getAttribute("user");
        }
        return sessionUser;
    }

    @Override
    public void beforePhase(PhaseEvent event) {

    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

}
