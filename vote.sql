-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2017 at 10:42 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vote`
--

-- --------------------------------------------------------

--
-- Table structure for table `choice`
--

CREATE TABLE `choice` (
  `id` int(11) NOT NULL,
  `playground` int(11) NOT NULL,
  `value` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `choice`
--

INSERT INTO `choice` (`id`, `playground`, `value`) VALUES
(28, 7, 'Luni'),
(29, 7, 'Miercuri'),
(30, 7, 'Marti'),
(31, 8, 'ciorba de burta'),
(32, 8, 'paste'),
(33, 8, 'shaorma'),
(43, 12, 'McDonalds'),
(44, 12, 'Pizza hut'),
(45, 12, 'La placinte'),
(46, 13, 'Mov'),
(47, 13, 'Galben'),
(48, 13, 'Rosu'),
(49, 13, 'Negru'),
(50, 13, 'Albastru'),
(51, 13, 'Verde'),
(52, 13, 'Gri'),
(60, 17, 'Luni'),
(61, 17, 'Miercuri'),
(62, 17, 'Joi'),
(63, 17, 'Vineri'),
(67, 19, 'Marti ora 9'),
(68, 19, 'Miercuri ora 7'),
(69, 19, 'Luni ora 8'),
(70, 20, 'Luni'),
(71, 20, 'Marti'),
(72, 20, 'Miercuri'),
(73, 20, 'Joi'),
(74, 20, 'Vineri'),
(75, 21, 'Bulgaria'),
(76, 21, 'Austria'),
(77, 21, 'Italia'),
(78, 22, 'Beer\'o\'clock'),
(79, 22, 'Curtea berarilor'),
(80, 22, 'Oktoberfest'),
(81, 23, 'pompa'),
(82, 23, 'curatenie'),
(83, 23, 'paza');

-- --------------------------------------------------------

--
-- Table structure for table `playground`
--

CREATE TABLE `playground` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `creator` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `playground`
--

INSERT INTO `playground` (`id`, `name`, `creator`, `active`) VALUES
(7, 'My first poll', 36, 1),
(8, 'cina', 37, 1),
(12, 'Unde mancam?', 32, 1),
(13, 'Culoarea preferata', 32, 1),
(17, 'Pescuit saptamana viitoare', 32, 1),
(19, 'Fotbal', 38, 1),
(20, 'Fotbal in saptamana 23', 39, 1),
(21, 'Vacanta la ski', 39, 1),
(22, 'Ce berarie incercam in weekend', 39, 1),
(23, 'lucrari cartier', 41, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `password`, `email`) VALUES
(31, 'testPassword', 'testEmail'),
(32, '123', 'andrei'),
(33, '123', 'andrei2'),
(34, '123', 'andrei3'),
(35, '123', 'andrei5'),
(36, '123456', 'andreiNewTest'),
(37, '666', 'raluca'),
(38, '123', 'andrei6'),
(39, '12345', 'andrei.cristea@gmail.com'),
(40, '123', 'userDejaExistent'),
(41, 'c0druta', 'ccristea');

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE `vote` (
  `id` int(11) NOT NULL,
  `choice` int(11) NOT NULL,
  `nickname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vote`
--

INSERT INTO `vote` (`id`, `choice`, `nickname`) VALUES
(59, 28, 'Andrei'),
(60, 29, 'Andrei'),
(61, 29, 'Gogu'),
(62, 30, 'Gogu'),
(63, 28, 'Gigel'),
(64, 29, 'Gigel'),
(65, 30, 'Gigel'),
(66, 28, 'Gogu'),
(67, 28, 'Raluca'),
(68, 30, 'Raluca'),
(73, 31, 'gigi'),
(74, 32, 'gigi'),
(94, 43, 'Andrei'),
(95, 44, 'Andrei'),
(96, 45, 'Andrei'),
(97, 48, 'Andrei'),
(98, 43, 'Gogu'),
(101, 44, 'Gogu'),
(102, 43, 'Raluca'),
(103, 45, 'Raluca'),
(104, 47, 'Andrei'),
(105, 47, 'Gogu'),
(106, 50, 'Gogu'),
(107, 60, 'Andrei'),
(108, 62, 'Andrei'),
(109, 60, 'Raluca'),
(110, 61, 'Raluca'),
(111, 62, 'Raluca'),
(114, 63, 'George'),
(115, 62, 'George'),
(116, 60, 'Alex'),
(117, 61, 'Alex'),
(118, 62, 'Alex'),
(119, 63, 'Alex'),
(120, 44, 'Raluca'),
(124, 67, 'Gogu'),
(125, 68, 'Gogu'),
(126, 69, 'Gogu'),
(128, 70, 'Andrei'),
(133, 70, 'George'),
(135, 72, 'George'),
(138, 74, 'Andrei'),
(139, 70, 'Mihai'),
(143, 71, 'Grigore'),
(144, 72, 'Grigore'),
(148, 72, 'Aurel'),
(151, 71, 'Victor'),
(152, 72, 'Victor'),
(153, 73, 'Victor'),
(154, 74, 'Victor'),
(155, 70, 'Victor'),
(156, 72, 'Petre'),
(157, 73, 'Petre'),
(158, 71, 'Razvan'),
(163, 73, 'Andrei'),
(164, 71, 'Aurel'),
(166, 74, 'Grigore'),
(167, 71, 'Petre'),
(169, 70, 'Razvan'),
(173, 72, 'Daniel'),
(174, 70, 'Daniel'),
(175, 81, 'b1'),
(176, 82, 'B5'),
(183, 81, 'B9'),
(184, 82, 'B9'),
(185, 83, 'B9'),
(187, 81, 'b1'),
(188, 83, 'B5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `choice`
--
ALTER TABLE `choice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playground` (`playground`);

--
-- Indexes for table `playground`
--
ALTER TABLE `playground`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator` (`creator`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `choice` (`choice`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `choice`
--
ALTER TABLE `choice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `playground`
--
ALTER TABLE `playground`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `vote`
--
ALTER TABLE `vote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `choice`
--
ALTER TABLE `choice`
  ADD CONSTRAINT `choice1` FOREIGN KEY (`playground`) REFERENCES `playground` (`id`);

--
-- Constraints for table `playground`
--
ALTER TABLE `playground`
  ADD CONSTRAINT `playground1` FOREIGN KEY (`creator`) REFERENCES `user` (`id`);

--
-- Constraints for table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `vote2` FOREIGN KEY (`choice`) REFERENCES `choice` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
